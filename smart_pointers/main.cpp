/**
 * AUTHOR: Emilia Paredes
 * This file contains a simple interpretation for a pairs game.
 */

#include <iostream>
#include "smart_pointers.h"

int main(){

    /*
    #pragma region UNIQUE_POINTERS

    //wrong
    std::unique_ptr<new_class> my_class;
    // 
    //std::unique_ptr<new_class> my_class =  new new_class();
    //because the constructor is explicit 
    //when it is explicit you need to call the constructor explicitly 
    //there is no implicit constructor
    
    //yess
    std::unique_ptr<new_class> my_class{ new new_class()};
    
    //better
    std::unique_ptr<new_class> my_class = std::make_unique<new_class>();

    //copy and assign opperators are deleted 
    //this nono 
    //std::unique_ptr<new_class> other_class = my_class;

    #pragma endregion //!UNIQUE_POINTRES


    #pragma region UNIQUE_POINTERS



    #pragma endregion //!SHARED_POINTERS


    #pragma region SHARED_POINTERS

    

    #pragma endregion //!SHARED_POINTERS
    */

   #pragma region UNIQUE_POINTERS

   #pragma endregion



    return 0;
}