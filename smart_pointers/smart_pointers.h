/**
 * AUTHOR: Emilia Paredes
 * BRIEF: This file contains the definition and implementation for smart pointers
 */

//#pragma once 
//without move semantics

#include <new>

template <typename T>
class unique_pointer {
    private:
        T* pObject;
    public:
        unique_pointer( T* data){
            pObject = data;
        }

        ~unique_pointer(){
            delete pObject;
        }

        //working as a proper pointer 
        //obtain the pointer through the arrow 
        T* operator->() const{
            return pObject;
        }
        //derreference the pointer 
        T& operator*() const{
            return *pObject;
        }

        //remove copy semantics 
        //copy constructor
        unique_pointer(const unique_pointer<T>& rhs) = delete;
        //copy assignment
        unique_pointer<T>& operator= (const unique_pointer<T>& rhs) = delete;

        //no implicit conversions or copy intialization
        explicit operator bool() const {
            return pObject;
        }
};


class shared_pointer_counter {
    private:
        unsigned int uReferenceCounter; 
    public:
        shared_pointer_counter()
            : uReferenceCounter(0)
        {}

        unsigned int get() const {
            return uReferenceCounter;
        }

        //post and pre increment
        void operator++(){
            uReferenceCounter++;
        }
        void operator++(int){
            uReferenceCounter++;
        }

        //post and pre decrement
        void operator--(){
            uReferenceCounter--;
        }
        void operator--(int){
            uReferenceCounter--;
        }
};

template<typename T>
class shared_pointer {
    private:
        shared_pointer_counter* pCounter;
        T* pObject;

        //deallocation if the counter is 0
        void dealocate(){
            (*pCounter)--;
            if(pCounter->get() == 0){
                delete pCounter;
                delete pObject;
            }
        }
    public:
        //constructor 
        //no implicit conversions or copy intialization
        explicit shared_pointer(T* data){
            pObject = data;
            pCounter = new (std::nothrow) shared_pointer_counter();
            //safe check and deletion if necesary 
            if(pCounter == nullptr){
                delete pObject;
                throw std::bad_alloc();
            }
            //if the data is not a nullptr then increase the counter
            if(data)
                (*pCounter)++;
        }

        shared_pointer(const shared_pointer<T>& rhs){
            swap(*this, rhs);
            (*pCounter)++;
        }
        
        //copy assignment
        shared_pointer<T>& operator= (shared_pointer<T> rhs){
            //apply copy & swap idiom 
            swap(*this, rhs);
            return *this;
        }

        ~shared_pointer(){
            dealocate();
        }

        //working as a proper pointer 
        //obtain the pointer through the arrow 
        T* operator->() const{
            return pObject;
        }
        //derreference the pointer 
        T& operator*() const{
            return *pObject;
        }

        //counter getter 
        unsigned int get_reference_counter() const {
            return pCounter->get();
        }

        // friend: adl (version to not specify std::swap function for this class)
        // copy & swap idiom 
        friend void swap(shared_pointer<T>& lhs, shared_pointer<T>& rhs){
            //adl approach 
            using std::swap;

            //swap information with std swap 
            std::swap(lhs.pObject, rhs.pObject);
            std::swap(lhs.pCounter, rhs.pCounter);
        } 
};
