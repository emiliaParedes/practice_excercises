#pragma once 

#include <iostream>

#pragma region IsUniqueExcercise

void IsUnique();

bool IsUnique_Forward(const std::string& str);
bool IsUnique_ForwardBits(const std::string& str);

#pragma endregion

#pragma region URLifyEx

void URLify();

void URLIfy_TwoScan(std::vector<char>& str);

#pragma endregion