#include "arrays_strings.h"
#include <vector>


#pragma region IsUniqueExcercise

#define MAX_ASCII_CHARS 256

// EX: #1 Is unique.
//  determine if a string has all unique characters 

void IsUnique(){
    std::cout << "-----------------Forward------------------" << std::endl;
    std::cout << IsUnique_Forward("HELLOASJDHASKJD") << std::endl;
    std::cout << IsUnique_Forward("a") << std::endl;
    std::cout << IsUnique_Forward("12345678901") << std::endl;
    
    std::cout << "--------------Forward Bits------------------" << std::endl;
    std::cout << IsUnique_ForwardBits("HELLOASJDHASKJD") << std::endl;
    std::cout << IsUnique_ForwardBits("a") << std::endl;
    std::cout << IsUnique_ForwardBits("12345678901") << std::endl;
}

//first approach 
//assuming ascii
//extra memory of 255 bools
bool IsUnique_Forward(const std::string& str){
    if(str.length() > MAX_ASCII_CHARS)
        return false;

    std::vector<bool> characters{};
    characters.resize(MAX_ASCII_CHARS);

    for(const char& c : str){
        const short position = static_cast<short>(c);
        if(characters[position])
            return false; 
        characters[position] = true;
    }
    return true;
}

//second 
//assuming ascii but only lowecase letter
//just an int as extra memory 
bool IsUnique_ForwardBits(const std::string& str){
    if(str.length() > MAX_ASCII_CHARS)
        return false;

    int flag = 0;
    for( const char& c : str){
        const short position = static_cast<short>(c) - 'a';
        if( (flag & (1 << position)) > 0 )
            return false; 
        flag |= (1 << position);
    }
    return true;
}

#pragma endregion

#pragma region URLifyEx

void URLify(){
    std::vector<char> first{};
    for(const char& c : first)
        std::cout << c;
    
    std::cout << std::endl;
    std::cout << URLIfy_TwoScan(first, 0) << std::endl;
}

void URLIfy_TwoScan(std::vector<char>& str, const int true_length){
    int space_counter = 0;
    for(int i = 0; i < true_length; i){
        if(str[i] == ' ')
            space_counter++;
    }

    int real_string_counter = true_length;

    //sanity check 
    if( (real_string_counter + space_counter * 2) > str.size())
        return; 

    for(size_t i = str.size() - 1; i >= 0; i--){
        if(str[real_string_counter] == ' '){
           // str[]
        }


    }
}

#pragma endregion