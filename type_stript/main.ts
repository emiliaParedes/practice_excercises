/*
types 

number 
string 
boolean
*/

function add_numbers(lhs: number, rhs: number){
    return lhs + rhs;
}

console.log("Hello this is TypeScript!");

//good
const number0 = 10;
//not necesary
const number1 : number = 0.6;
//good
let   number2 : number;
number2 = 2.6; //number2 = '2.3' gives error

const result = add_numbers(number0, number1);
console.log("Result is: " + result); 


//objects 
//const person: {
//    name: string;
//    age: number;
//} = {
const person = {
    name: 'Hello',
    age: 12
};

// error since it doesnt exist 
//person.nickname;

console.log(person.name);



//arrays 
const other_person = {
    name: "World",
    age: 12,
    //good
    hobbies: ["paint", "sing"]
    //error, no mixed arrays
    //hobbies: ["paint", 1.0]
};

for (const hobby of other_person.hobbies){
    //good since it knows its a string
    console.log(hobby.toUpperCase());
}




//tuple 
const another_person:{
    name : string;
    age: number;
    work: [number, string];
} = {
    name: "!!",
    age: 12,
    //good
    //an exact array with certain element types and certain size
    //[number, string]
    work: [2, "doctor"]
};


//this is an exception
another_person.work.push("engineer");
//work: [2, "doctor", "engineer"]

//this cant be done 
another_person.work = [2, "doctor", "engineer"];




//enum 

enum EWork { eDoctor = 'hello',
             eEngineer = 0, 
             ePilot = 100};

const different_person = {
    name: "World",
    age: 12,
    //good
    work:  EWork.eDoctor
};







//union 
function combine0(lhs : number | string, rhs : number | string, to_output : "number" | "text") {
    var result = 0;
    if(typeof lhs === 'number' && typeof rhs === 'number')
        result = lhs + rhs;
    return result;
}

type combinable = number | string 
function combine1(lhs : combinable, rhs : combinable, to_output : "number" | "text") {
    var result = 0;
    if(typeof lhs === 'number' && typeof rhs === 'number')
        result = lhs + rhs;
    return result;
}








//function return                       : void
function add0(lhs: number, rhs: number) : number{
    return lhs + rhs;
}

//undefined 
let a_var : undefined;
function add1(lhs: number, rhs: number) : undefined{
    console.log(lhs + rhs);
    //if no return, then error
    return;
}

//function pointers 
//whichever function
let a_function0 : Function = add0;
//pointer to func that takes no param and returns a number 
let a_function1 : () => number;
//pointer to func that takes 2 number param and returns a number 
let a_function2 : (a :number, b :number) => number;





