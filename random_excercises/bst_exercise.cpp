/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>

using namespace std;

struct node{
    int value = 0;
    node* right = nullptr;
    node* left= nullptr;
};

struct tree{
    node* root = nullptr;
};

void update_with_higher_rec(node* current_node, int& max_value){
    //base case
    if(current_node == nullptr)
        return;
    //update max value
    if(current_node->value > max_value)
        max_value = current_node->value;
    //now keep traversing
    update_with_higher_rec(current_node->right, max_value);
    //we have reached the first max element 
    //not adding itself
    if(max_value == current_node->value)
        return;
    
    current_node->value += max_value;
    
    if(current_node->value > max_value)
        max_value = current_node->value;
    
    update_with_higher_rec(current_node->left, max_value);
}


void update_with_higher(tree* c_tree){
    int max_val = c_tree->root->value;
    update_with_higher_rec(c_tree->root, max_val);
}


void print_tree(node* c_node){
    if(c_node == nullptr)
        return;
    std::cout << "node: " << c_node->value << std::endl;
    print_tree(c_node->left);
    print_tree(c_node->right);
}



int main()
{
    tree new_tree{};
    node n80{};
    n80.value = 80;
    node n60{};
    n60.value = 60;
    node n70{};
    n70.value = 70;
    n70.right = &n80;
    n70.left = &n60;
    node n40{};
    n40.value = 40;
    node n20{};
    n20.value = 20;
    node n30{};
    n30.value = 30;
    n30.right = &n40;
    n30.left = &n20;
    node n50{};
    n50.value = 50;
    n50.right = &n70;
    n50.left = &n30; 


    new_tree.root = &n50;
    
    print_tree(new_tree.root);
    update_with_higher(&new_tree);
    std::cout << std::endl;
    print_tree(new_tree.root);
    return 0;
}




